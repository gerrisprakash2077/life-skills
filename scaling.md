# Scaling

# Scenario:

You joined a new project. The project is going through some performance and scaling issues.After some analysis, the team lead asks you to investigate the possiblity of using load balancers and understand vertical/horizontal scaling solutions.

## Load Balancer

Load balancers are used to distribute incomming client requests across multiple servers.

Using load balancers can improve performance and can handle more number of client requests as it distributes the load to multiple servers. 

### Types Of Load Balancers
* Software load balancers in client - All the logic of load balancing will be in the client application and the application will be provided with the list of servers and the application decides which server to connect.
* Software load balancers in services -These load balancers are external software that has the logic to select the server.
* Hardware load balancers - A specific hardware that distributes the load across the servers. 

### Load Balancing Algorithms

* Round Robin -Client requests are distributed across the servers sequentially.
* Least Connections-Sends client request to the server with the fewest current connection to client.
* Least Response Time-Sends client request to the server which is selected by calculating fastest response time and fewest active connections.
* IP Hash-Sends client request to the server based on IP address of the client.
* Random With Two Choices- Picks two server randomly and sends the request to the server with least connection.

Since the project is going through performance and scaling issues, we can implement least connections or least response time load balancing algotithm, as this will increase the performance by equally distributing the loads across the server.

![](loadbalancers.png)

## Horizontal And Vertical Scaling

Scaling up the server can be done to increase performance and to handle more client requests.

### Vertival Scaling

When new resources are added in the existing system to meet the expectations, it is known as vertical scaling. Vertival scaling keeps your existing infrastructure but adds computing power.

#### Advantages:
* Cheaper than horizontal scaling.
* Easy maintainance as we are not adding new servers.

#### Disadvantages:
* The risk of downtime is much higher than horizontal scaling.
* There is a limit for how much you can upgrade a single machine. 

### Horizontal Scaling

When new server racs are added in the existing system to meet the expectations, it is known as horizontal scaling.

#### Advantages:
* Less risk during server downtime.
* Low latency.
* Horizontal scaling increases availablity because as long as we are spreading our infrastructure across multiple areas, if one machine fails we can use the other one.
#### Disadvantages:
* Costlier than vertical scaling.

Implementing vertical scaling or horizontal scaling is based on the scale of the project, as both horizontal and vertical scaling has advantages and disadvantages

If the project already has a good infrastructure of severs but low computing power, implementing vertical scaling will be a good idea.






![](horizontal-vs-vertical-scaling.png)

## References

* https://youtu.be/sCR3SAVdyCc
* https://www.geeksforgeeks.org/load-balancer-system-design-interview-question/
* https://www.geeksforgeeks.org/horizontal-and-vertical-scaling-in-databases/
