# Listening and Active Communication
## 1.What are the steps/strategies to do Active Listening? (Minimum 6 points)

* Avoid getting distracted by your own thoughts.
* Focus on the speaker and topic.
* Try not to interrupt the other person. 
* Let them finish and then respond.
* Show that you are listening with your body language.
* If appropriate, take notes during important conversations.

## According to Fisher's model, what are the key points of Reflective Listening? (Write in your own words, use simple English)

* Actively engaging in the conversation.
* Genuinely listening from the speaker's point of view.

## What are the obstacles in your listening process?

* Getting distracted while the other person is speaking.
* Most of the time, I will interrupt the other person.

## What can you do to improve your listening?

* Avoid getting distracted by my own thoughts.
* Try not to interrupt the other person. 

## When do you switch to Passive communication style in your day to day life?

When I am meeting a person for the first time.

## When do you switch into Aggressive communication styles in your day to day life?

While playing competitive valorant with my friends.

## When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?

When I am bored by the person speaking with me.

## How can you make your communication assertive? You can analyse the videos and then think what steps you can apply in your own life?

* Pay attention to the speaker.
* Genuinely listening from the speaker's point of view.
* Give me your honest opinion on my ideas.
* Avoid interrupting the speaker.



