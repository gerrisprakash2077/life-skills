# Focus Management

## 1. What is Deep Work?

Doing hard work with full focus for a long time is called "deep work."

## 2. Paraphrase all the ideas in the above videos and this one in detail.


* The first video says the optimal duration of deep work is at least 1 hour.

* The second video says having a deadline reduces the frequency of taking breaks and improves speed.

* The third video says when we practise deep work, we upgrade our brain, and that enables us to focus more and complete difficult tasks.


## 3. How can you implement the principles in your day to day life?

* Practise deep work for at least a one hour stretch.

* Remove distractions.

* Make deep work as a habbit.

## 5. Your key takeaways from the video

* It is not necessary to have social media to connect with people and friends.
 
* Avoiding social media will increase your productivity.

* A moment on social media will disturb your deep work.

* Social media gives us entertainment, but that's not true.





