# Grit and Growth Mindset

## 1.Paraphrase (summarize) the video in a few lines. Use your own words.

* Grit is passion and preseverance for very long-term goals. 
* Grit is having stamina and it is sticking with your future to work hard and making the future a reality. 



## 2.What are your key takeaways from the video to take action on?

* Never take a failure as a permanent decision.

## 3.Paraphrase (summarize) the video in a few lines in your own words.

### Fixed mindset :


* Fixed mindset people believe that skill and intelligence are set whether you have them or not.
* Fixed mindset people believe that skills are born.


### Growth mindset :


* Growth mindset people believe that they can learn any skills and practice them to master them.
* Growth mindset people believe skills can be learned.

## 4.What are your key takeaways from the video to take action on?

* Video talks about the importance of growth mindset and Fixed mindset people.
* Fixed mindset people believe that skills and intelligence are something you are born with and cannot be improved.
* Growth mindset people believe that skills are something that you can get good at by practicing and becoming the master of it.

## 5.What is the Internal Locus of Control? What is the key point in the video?

Locus of Control refers to an individual's perception of the underlying
main causes of events in his/her life. Or, more simply:
Do you believe that your destiny is controlled by yourself or by external
forces?

## 6.Paraphrase (summarize) the video in a few lines in your own words.

The video talks about how we can develop our grit and growth mind. The first to believe in your abilities. The fundamental rule is to believe in your abilities and always try to learn new skills with a growth mindset. to believe create a mental structure.

## 7.What are your key takeaways from the video to take action on?

* Believe in your abilities to figure out whatever things you are stuck with.
* Always question your mind and the thoughts are they negative or positive?
* If you are struggling with something do not get discouraged and try to reach the result.

## 8.What are one or more points that you want to take action on from the manual? (Maximum 3)

* I will be confident.
* I will stay relaxed and focused no matter what happens.
* I will use Documentation, Google, Stack Overflow, Github Issues and Internet before asking help from fellow warriors or look at the solution.
