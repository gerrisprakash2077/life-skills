# The Learning Procedure


## 1. What is the Feynman Technique? Paraphrase the video in your own words.



The Feyman technique is used to check the depth of understanding of a particular concept.



The video explains Feyman's technique for learning a concept in depth. The procedure is as follows:


* Write the concept's name on a piece of paper.
* Describe the concept in simple terms.
* Identify the problem area, then check back with the source for review.
* Identify difficult areas and simplify yourself.



## 2. What are the different ways to implement this technique in your learning process?
* Ask myself questions about the concept I'm learning.
* Explaining the concept to myself in my mind and writing down the explanation on a piece of paper.


## 3: Describe the video in your own words in detail.
The video is about how to learn.
* Some biographies of Barbara Oakley
* How the brain works in focused and relaxed modes
* How we can use the benefits of both modes to learn more quickly.
## 4. What are some of the steps that you can take to improve your learning process?


* If I was about to learn a new concept, then I had to do it in a relaxed way.
* To master the concept, practise it repeatedly with complete focus.


## 5. What are your main takeaways from watching the video?Paraphrase your understanding.


* It takes 20 hours to learn something new at a good level of understanding, but it takes 10,000 hours to become an industry expert.
It also explains how to learn in 20 hours.


## 6. What are some of the steps that you can take while approaching a new topic?
* Disassemble the skills.
* Remove any barriers to practice.
* Practice for at least 20 hours.

