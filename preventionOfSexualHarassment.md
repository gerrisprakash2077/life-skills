# Sexual Harassment Prevention


## 1. What kinds of behaviour cause sexual harassment?

* Eve teasing, unwelcome invitations meet outside office, suggestive comments or jokes, physical confinement against one’s will and intruding one’s privacy.
* Any inappropriate conduct or comment by a person toward another person.
* Spreading sexually explicit rumours about a coworker.
* Making inappropriate jokes.
* Inappropriate gestures.
* Anything that would humiliate the coworker.
* Demand or request for sexual favours.
* Undesirable physical, verbal or non-verbal conduct of sexual nature.
* Staring, leering or unwelcome touching.

## 2. What would you do in case you faced or witnessed any incident or repeated incidents of such behaviour?

* Let the person know it is inappropriate.
* File a complaint about the incident in accordance with the employer's policy.


