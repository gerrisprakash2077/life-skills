# What is your one major takeaway from each one of the 6 sections. So 6 points in total.


* Be clear while gathering requirments.


* Use the group chat/channels to communicate most of the time. This is preferable over private DMs simply because there are more eyes on the main channel.


* Explain the problem clearly, mention the solutions you tried out to fix the problem


* Try to find out relevant team member's work schedules, and set up a call when they are free


* Be available when someone replies to your message. Things move faster if it's a real-time conversation as compared to a back and forth every hour.


* Deep work when you work, play when you play is a good thumb rule.



# Which area do you think you need to improve on? What are your ideas to make progress in that area?

## Which area do you think you need to improve on?

Being clear while communicating with a person.

## What are your ideas to make progress in that area?

I will try to be as clear as possible without confusing anything.


